// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2011 - DIGITEO - Michael Baudin
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

//
// %linpro_string --
//   Returns the string containing the linpro component.
//
function str = %linpro_string ( this )
    str = []
    k = 1
    str(k) = sprintf("LINPRO Object:\n")
    k = k + 1
    str(k) = sprintf("===========")
    k = k + 1
    str(k) = ""
    k = k + 1
    str(k) = sprintf("p: %s\n", _tostring(this.p));
    k = k + 1
    str(k) = sprintf("C: %s\n", _tostring(this.C));
    k = k + 1
    str(k) = sprintf("b: %s\n", _tostring(this.b));
    k = k + 1
    str(k) = sprintf("ci: %s\n", _tostring(this.ci));
    k = k + 1
    str(k) = sprintf("cs: %s\n", _tostring(this.cs));
    k = k + 1
    str(k) = sprintf("mi: %s\n", _tostring(this.mi));
    k = k + 1
endfunction

//
// _strvec --
//  Returns a string for the given vector.
//
function str = _strvec ( x )
    str = strcat(string(x)," ")
endfunction
function s = _tostring ( x )
  if ( x==[] ) then
    s = "[]"
  else
    n = size ( x , "*" )
    if ( n == 1 ) then
      s = string(x)
    else
	  [nr,nc] = size(x)
	  tx = typeof(x)
      s = msprintf("%d-by-%d %s matrix",nr,nc,tx)
    end
  end
endfunction



